# Stock Price Prediction Using News Sentiment Analysis

## Assigment 
The goal of my semestral work was to train and test stock price prediction using news sentiment analysis and comparing results of two approaches to apply sentiment to the news, also comparing benefits of various reccurent network models. 

## Dataset 
For training the model is used two datasets: 
1) [News and Stock Prices](https://www.kaggle.com/aaron7sun/stocknews)
2) [ FinancialPhraseBank by Malo et al. (2014)](https://www.researchgate.net/publication/251231107_Good_Debt_or_Bad_Debt_Detecting_Semantic_Orientations_in_Economic_Texts)
    (Download of the data is under Supplementary resource)
    

## Run the experiments

1.  Download both datasets to extract them to the `data` folder 
2.  Install `requrements.txt`
3.  In case of decent amount of computational resources follow 3.1 othervise 3.2
    1.    run jupyter notebook `Train_Financial_BERT.ipynb` 
    2.    Move to `Sentences_75Agree.txt` to the specified data folder in G-drive and run in Google Colab, download the saved train model to the `data` folder
4.  Run `run.py`



### Report [here](https://gitlab.fit.cvut.cz/nykodmar/mvi-sp/blob/master/report.pdf)

### Medium article [here](https://nykodmar.medium.com/stock-price-prediction-using-news-sentiment-analysis-74142a31885e)
