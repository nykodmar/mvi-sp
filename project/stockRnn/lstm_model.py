# Some parts used from examples provided at  https://www.tensorflow.org/tutorials/structured_data/time_series
import tensorflow as tf


class ReccurentNetModel(tf.keras.Model):

    def __init__(self, cell, out_steps, num_features):
        super().__init__()
        self.out_steps = out_steps
        self.lstm_cell = cell

        # Wrap the cell in an RNN (easier warmup)
        self.lstm_rnn = tf.keras.layers.RNN(self.lstm_cell, return_state=True)
        # Convert LSTM outputs to prediction
        self.dense = tf.keras.layers.Dense(num_features)

    @property
    def model_name(self):
        cell_type = self.lstm_cell.name.split("_")[0]
        if cell_type == "simple":
            cell_type = "rnn"
        return f"{cell_type}({self.lstm_cell.units})"

    def warmup(self, inputs):
        '''Inicialization of internal states before first prediction'''

        x, *state = self.lstm_rnn(inputs)

        # predictions.shape => (batch, features)
        prediction = self.dense(x)

        return prediction, state

    def call(self, inputs, training=None):
        '''After warmup use last prediction as
            input for next predictions
        '''
        predictions = []

        # Initialize the lstm state and predict first
        prediction, state = self.warmup(inputs)

        predictions.append(prediction)

        # Run the rest of the prediction steps
        for n in range(1, self.out_steps):
            # Use the last prediction as input.
            x = prediction
            # Execute one lstm step.
            x, state = self.lstm_cell(x, states=state,
                                      training=training)
            # Convert the lstm output to a prediction.
            prediction = self.dense(x)
            predictions.append(prediction)

        # Stack and transpose prediction to correct order
        predictions = tf.stack(predictions)
        predictions = tf.transpose(predictions, [1, 0, 2])

        return predictions
