import tensorflow as tf
import numpy as np
import matplotlib.pyplot as plt
import itertools
import json

from tensorflow import keras
from dataset_loader import DatasetLoader
from preprocessor import get_preprocessed_datasets
from lstm_model import ReccurentNetModel
from results_visualizator import plot_model_mae, plot_model_loss, plot_results, plot_best_results



MAX_EPOCHS = 64
INPUT_WIDTH = 30

sentiment_performance = {}
val_sentiment_performance = {}

def compile_and_fit(model, window, patience=10):
    early_stopping = tf.keras.callbacks.EarlyStopping(monitor='val_loss',
                                                      patience=patience,
                                                      mode='min')

    model.compile(loss=tf.losses.MeanSquaredError(),
                  optimizer=tf.optimizers.Adam(),
                  metrics=[tf.metrics.MeanAbsoluteError()])

    history = model.fit(window.train, epochs=MAX_EPOCHS,
                        validation_data=window.val,
                        callbacks=[early_stopping])
    return history



def train_valid_test_split(df):
    ''':returns  (70:20:10) train, validation and test '''

    n = len(df)
    train_df = df[0:int(n * 0.7)]
    val_df = df[int(n * 0.7):int(n * 0.9)]
    test_df = df[int(n * 0.9):]

    train_mean = train_df.mean()
    train_std = train_df.std()

    train_df = (train_df - train_mean) / train_std
    val_df = (val_df - train_mean) / train_std
    test_df = (test_df - train_mean) / train_std

    return train_df, val_df, test_df


def get_models(num_features, out_steps, num_units=None, use_best=False):
    '''Returns a list of models based on model_types. Either for hyper-parameter finding
        or final comparison and plotting'''

    models = []
    recurrent_cells = []

    # Define the cells used for the model
    if num_units:
        if use_best: # Simplification for faster results as GRU was best in all cases
            recurrent_cells = [tf.keras.layers.GRUCell(num_units)]
        else:
            recurrent_cells = [tf.keras.layers.GRUCell(num_units),
                               tf.keras.layers.LSTMCell(num_units),
                               tf.keras.layers.SimpleRNNCell(num_units)]
    else:
        for units in [16, 32, 64, 128, 256, 512]: # Used for finding the best num_units/cell type for the dataset
            recurrent_cells.append([tf.keras.layers.GRUCell(units), tf.keras.layers.LSTMCell(units), tf.keras.layers.SimpleRNNCell(units)])
            recurrent_cells = list(itertools.chain.from_iterable(recurrent_cells))


    # Create and return the models
    for cell in recurrent_cells:
        model = ReccurentNetModel(cell, out_steps, num_features )
        models.append(model)

    return models


def get_dataset_loader(df, ds_type, days_predicted):

    train_df, val_df, test_df = train_valid_test_split(df)

    ds_loader = DatasetLoader(input_width=INPUT_WIDTH,
                                label_width=days_predicted,
                                shift=days_predicted,
                                train_df=train_df, val_df=val_df, test_df=test_df,
                                label_columns=[ds_type]
                                )

    return ds_loader


def evaluate_models_on_dataset(df, ds_type, sentiment_type, days_predicted=2, num_units=None, use_best=False):

    val_performance = {}
    performance = {}

    ds_loader = get_dataset_loader(df, ds_type, days_predicted)

    num_features = df.shape[1]
    models = get_models(num_features, days_predicted,num_units, use_best)

    for i, model in enumerate(models):
        history = compile_and_fit(model, ds_loader)
        val_performance[model.model_name] = model.evaluate(ds_loader.val)
        performance[model.model_name] = model.evaluate(ds_loader.test, verbose=0)

        print(f"Training model {model.model_name} on {sentiment_type} dataset ({i}/{len(models)})")
        print(f"MAE: {performance[model.model_name][1]}")

        # Plot loss & error results
        plot_model_mae(history, model.model_name, ds_type, sentiment_type)
        plot_model_loss(history, model.model_name, ds_type, sentiment_type)
        # Plot predictions
        ds_loader.plot(model, ds_type, days_predicted)

    # Plot performances
    top_performing_model = min(performance, key=performance.get)
    sentiment_performance[f"{sentiment_type}_{top_performing_model}"] = performance[top_performing_model]
    val_sentiment_performance[f"{sentiment_type}_{top_performing_model}"] = val_performance[top_performing_model]
    plot_results(performance, val_performance, ds_type, 1, sentiment_type)



def evaluate_models_on_sentiment_types(vanilla_ds, vader_ds, bert_ds, ds_type):

    evaluate_models_on_dataset(vader_ds, ds_type, "vader")
    evaluate_models_on_dataset(bert_ds, ds_type, "bert")
    evaluate_models_on_dataset(vanilla_ds, ds_type, "vanilla")

    plot_best_results(sentiment_performance, val_sentiment_performance, ds_type, 1)


def run():

    ds_type = "close"
    vader_ds, bert_ds = get_preprocessed_datasets(ds_type)
    vanilla_ds = vader_ds[[ds_type]]

    evaluate_models_on_sentiment_types(vanilla_ds, vader_ds, bert_ds, ds_type)
    plot_best_results(sentiment_performance, val_sentiment_performance, ds_type, 1)


# run()