from matplotlib import pyplot as plt
import numpy as np
import json
OUTPUT_DATA = "../output_data"

'''This module serves only to plot/save the results'''

def plot_model_mae(history, model_name, ds_type, sentiment_type):

    plt.plot(history.history['mean_absolute_error'])
    plt.plot(history.history['val_mean_absolute_error'])
    plt.title(f'{model_name} MAE on {ds_type}({sentiment_type}) dataset')
    plt.ylabel('MAE')
    plt.xlabel('epoch')
    plt.legend(['train', 'val'], loc='upper left')
    plt.savefig(f"{OUTPUT_DATA}/{ds_type}_accuracy_{model_name}_{sentiment_type}.png")
    plt.savefig(f"{OUTPUT_DATA}/{ds_type}_accuracy_{model_name}_{sentiment_type}.pdf")

    plt.clf()

def plot_model_loss(history, model_name, ds_type, sentiment_type):

    plt.plot(history.history['loss'])
    plt.plot(history.history['val_loss'])
    plt.title(f'{model_name} loss on {ds_type} ({sentiment_type}) dataset')
    plt.ylabel('loss')
    plt.xlabel('epoch')
    plt.legend(['train', 'val'], loc='upper left')
    plt.savefig(f"{OUTPUT_DATA}/{ds_type}_loss_{model_name}_{sentiment_type}.png")
    plt.savefig(f"{OUTPUT_DATA}/{ds_type}_loss_{model_name}_{sentiment_type}.pdf")
    plt.clf()

def plot_results(performance, val_performance, ds_type, metric_index, sentiment_type):

    x = np.arange(len(performance))
    width = 0.3

    val_mae = [v[metric_index] for v in val_performance.values()]
    test_mae = [v[metric_index] for v in performance.values()]

    plt.title(f'{ds_type} MAE error on {sentiment_type} dataset')
    plt.ylabel(f'mean_absolute_error [{ds_type}]')
    plt.bar(x - 0.17, val_mae, width, label='Validation')
    plt.bar(x + 0.17, test_mae, width, label='Test')
    plt.xticks(ticks=x, labels=performance.keys())
    _ = plt.legend()
    file_name = f"{OUTPUT_DATA}/{ds_type}_{sentiment_type}"
    plt.savefig(file_name + ".png")
    plt.savefig(file_name + ".pdf")

    json.dump(val_performance, open(file_name + "_val.json", 'w'))
    json.dump(performance, open(file_name + ".json", 'w'))

    plt.clf()


def plot_best_results(performance, val_performance, ds_type, metric_index):

    x = np.arange(len(performance))
    width = 0.3

    val_mae = [v[metric_index] for v in val_performance.values()]
    test_mae = [v[metric_index] for v in performance.values()]

    plt.title(f'MAE error on {ds_type} dataset')
    plt.ylabel(f'mean_absolute_error [{ds_type}]')
    plt.bar(x - 0.17, val_mae, width, label='Validation')
    plt.bar(x + 0.17, test_mae, width, label='Test')
    plt.xticks(ticks=x, labels=performance.keys())
    _ = plt.legend()


    file_name = f"{OUTPUT_DATA}/best_{ds_type}_comparison"
    plt.savefig(file_name + ".png")
    plt.savefig(file_name + ".pdf")

    json.dump(val_performance, open(file_name + "_val.json", 'w'))
    json.dump(performance, open(file_name + ".json", 'w'))

    plt.clf()

