# Some parts used from examples provided at https://www.tensorflow.org/tutorials/structured_data/time_series
import pandas as pd
import matplotlib.pyplot as plt
import tensorflow as tf
from tensorflow import keras
import numpy as np

class DatasetLoader():
    '''Handle the indexes and offsets
        efficiently represent batches
    '''

    def __init__(self, input_width, target_width, shift,
                 train_df, val_df, test_df,
                 target_columns=None):
        ''' :param input_width: # of timesteps before prediction
            :param target_width: # of predicted timesteps
            :param shift: # of timesteps between ends of input_width and target_width
            :param target_columns: # names of target columns

        '''
        # Store the raw data.
        self.train_df = train_df
        self.val_df = val_df
        self.test_df = test_df

        # Work out the target column indices.
        self.target_columns = target_columns
        if target_columns is not None:
            self.target_columns_indices = {name: i for i, name in
                                          enumerate(target_columns)}
        self.column_indices = {name: i for i, name in
                               enumerate(train_df.columns)}

        # Work out the window parameters.
        self.input_width = input_width
        self.target_width = target_width
        self.shift = shift

        self.total_window_size = input_width + shift

        self.input_slice = slice(0, input_width)
        self.input_indices = np.arange(self.total_window_size)[self.input_slice]

        self.target_start = self.total_window_size - self.target_width
        self.targets_slice = slice(self.target_start, None)
        self.target_indices = np.arange(self.total_window_size)[self.targets_slice]

    @property
    def train(self):
        return self.make_dataset(self.train_df)

    @property
    def val(self):
        return self.make_dataset(self.val_df)

    @property
    def test(self):
        return self.make_dataset(self.test_df)

    @property
    def example(self):
        """Get and cache an example batch of `inputs, targets` for plotting."""
        result = getattr(self, '_example', None)
        if result is None:
            result = next(iter(self.train))
            # Cache it for next time
            self._example = result

        return result


    def make_dataset(self, data):
        data = np.array(data, dtype=np.float32)
        ds = tf.keras.preprocessing.timeseries_dataset_from_array(
            data=data,
            targets=None,
            sequence_length=self.total_window_size,
            sequence_stride=1,
            shuffle=True,
            batch_size=32, )

        ds = ds.map(self.split_window)

        return ds


    def split_window(self, features):
        inputs = features[:, self.input_slice, :]
        targets = features[:, self.targets_slice, :]
        if self.target_columns is not None:
            targets = tf.stack(
                [targets[:, :, self.column_indices[name]] for name in self.target_columns],
                axis=-1)

        inputs.set_shape([None, self.input_width, None])
        targets.set_shape([None, self.target_width, None])

        return inputs, targets





    def plot(self, model=None, plot_col='close', day_precicted=1, max_subplots=3):
        '''Plot predictions vs real targets'''

        inputs, targets = self.example
        plt.figure(figsize=(12, 8))
        plot_col_index = self.column_indices[plot_col]
        max_n = min(max_subplots, len(inputs))
        for n in range(max_n):
            plt.subplot(3, 1, n + 1)
            plt.ylabel(f'{plot_col} [normed]')
            plt.plot(self.input_indices, inputs[n, :, plot_col_index],
                     target='Inputs', marker='.', zorder=-10)

            if self.target_columns:
                target_col_index = self.target_columns_indices.get(plot_col, None)
            else:
                target_col_index = plot_col_index

            if target_col_index is None:
                continue

            plt.scatter(self.target_indices, targets[n, :, target_col_index],
                        edgecolors='k', label='Targets', c='#2ca02c', s=64)
            if model is not None:
                predictions = model(inputs)
                plt.scatter(self.target_indices, predictions[n, :, target_col_index],
                            marker='X', edgecolors='k', label='Predictions',
                            c='#ff7f0e', s=64)

            if n == 0:
                plt.legend()

        plt.xlabel('Time [days]')
        plt.savefig(f'../output_data/{model.model_name}_{day_precicted}.pdf')
        plt.savefig(f'../output_data/{model.model_name}_{day_precicted}.png')
        plt.clf()


