import pandas as pd
import re
import os.path
import pickle

# Vader
from nltk.sentiment.vader import SentimentIntensityAnalyzer
import nltk

# BERT
import torch
import torch.nn.functional as F
from transformers import BertForSequenceClassification, BertTokenizer
import numpy as np

BERT_FINANCIAL_MODEL = "../data/BERT_sentiment_banking_data"

NEWS_DATA_PATH = "../data/Combined_News_DJIA.csv"
STOCKS_DATA_PATH = "../data/upload_DJIA_table.csv"

CLOSE_VADER_PROCESSED_DATA = "../data/close_vader_processed_data.pkl"
MVMNT_VADER_PROCESSED_DATA = "../data/movement_vader_processed_data.pkl"

CLOSE_BERT_PROCESSED_DATA = "../data/close_bert_processed_data.pkl"
MVMNT_BERT_PROCESSED_DATA = "../data/movement_bert_processed_data.pkl"

DATASETS = [CLOSE_VADER_PROCESSED_DATA, MVMNT_VADER_PROCESSED_DATA, CLOSE_BERT_PROCESSED_DATA, MVMNT_BERT_PROCESSED_DATA]

def preprocess_headlines(df):
    # Clean the news text
    top_cols = []
    for col in df:
        if col.startswith("Top"):
            top_cols.append(col)
            df[col] = df[col].apply(lambda x: re.sub('b"|b\'|\\\\|\\"', "", str(x)))

    # Prepare joined headlines column
    df['top_vader'] = df[
        ['Top1', 'Top2', 'Top3', 'Top4', 'Top5', 'Top6', 'Top7', 'Top8', 'Top9', 'Top10', 'Top11', 'Top12', 'Top13',
         'Top14', 'Top15', 'Top16', 'Top17', 'Top18', 'Top19', 'Top20', 'Top21', 'Top22', 'Top23', 'Top24',
         'Top25']].agg(' '.join, axis=1)

    df['top_bert'] = df[
        ['Top1', 'Top2', 'Top3', 'Top4', 'Top5', 'Top6', 'Top7', 'Top8', 'Top9', 'Top10', 'Top11', 'Top12', 'Top13',
         'Top14', 'Top15', 'Top16', 'Top17', 'Top18', 'Top19', 'Top20', 'Top21', 'Top22', 'Top23', 'Top24',
         'Top25']].agg(';'.join, axis=1)

    return df


def get_vader_sentiment_dataset(df):

    # Download lexicon
    nltk.download('vader_lexicon')

    # Evaluate combined news sentiment
    vader = SentimentIntensityAnalyzer()
    scores = df['top_vader'].apply(vader.polarity_scores).tolist()
    scores_df = pd.DataFrame(scores)
    df = df.join(scores_df, rsuffix='_right')
    df.reset_index(inplace=True)

    return df[["close", "movement", "neg", "pos", "neu", "compound"]]


def get_bert_sentiment_dataset(df):

    # Prepare headlines list for each day
    df['top_bert'] = df['top_bert'].apply(lambda x: x.split(";"))

    # Load BERT model on trained financial news sentiment dataset
    bert_model = BertForSequenceClassification.from_pretrained('bert-base-uncased', num_labels=3)
    bert_model.load_state_dict(torch.load(BERT_FINANCIAL_MODEL))
    device = 'cuda:0' if torch.cuda.is_available() else 'cpu'
    bert_model = bert_model.to(device)

    # Set to evaluation mode
    bert_model.eval()

    # Prepare tokenizer for the BERT model
    param_b_tk = {
        'return_tensors': "pt",
        'padding': 'max_length',
        'max_length': 110,  # Average length of news headline
        'add_special_tokens': True,
        'truncation': True
    }
    bert_tokenizer = BertTokenizer.from_pretrained('bert-base-uncased', do_lower_case=True)

    def get_average_news_sentiment(news, model, tokenizer, param_tk):
        """:param news: Is in this case headlines for one day
           :returns: Average sentiment of the day's headlines """

        with torch.no_grad():
            X = tokenizer(news, **param_tk).to(device)
            logits = model(**X)[0]

            pred = F.softmax(logits, dim=1).cpu().numpy()
            pred = np.mean(pred, axis=0)

        return pred

    df["sentiment_score"] = df.apply(
        lambda x: get_average_news_sentiment(x['top_bert'], bert_model, bert_tokenizer, param_b_tk), axis=1)

    # Split the predicted sentiment to columns and return only df with training columns
    df["neg"] = df["sentiment_score"].apply(lambda x: x[0])
    df["neu"] = df["sentiment_score"].apply(lambda x: x[1])
    df["pos"] = df["sentiment_score"].apply(lambda x: x[2])
    return df[["close", "movement", "neg", "neu", "pos"]]


def get_preprocessed_datasets(type="close"):
    """:returns: Sentiment evaluated datasets"""

    for dataset in DATASETS:
        if not os.path.isfile(dataset):
            run()

    if type == "close":
        return pd.read_pickle(CLOSE_VADER_PROCESSED_DATA), pd.read_pickle(CLOSE_BERT_PROCESSED_DATA)

    return pd.read_pickle(MVMNT_VADER_PROCESSED_DATA), pd.read_pickle(MVMNT_BERT_PROCESSED_DATA)


def run():
    # Load data to pandas dataframe
    news = pd.read_csv(NEWS_DATA_PATH)
    stocks = pd.read_csv(STOCKS_DATA_PATH)

    # Join stock data with news data and rename targets
    df = news.merge(stocks, on='Date')
    df.rename(columns={'Label': 'movement', 'Close': 'close'}, inplace=True)

    # Get preprocessed headlines and join to single column
    df_vader = preprocess_headlines(df)
    df_bert = preprocess_headlines(df)

    # Evaluate sentiment of the Headlines using Vader and save
    vader_ds = get_vader_sentiment_dataset(df_vader)
    vader_ds.drop("movement", axis=1).to_pickle(CLOSE_VADER_PROCESSED_DATA)
    vader_ds.drop("close", axis=1).to_pickle(MVMNT_VADER_PROCESSED_DATA)

    # Evaluate sentiment of the Headlines using BERT and save
    bert_ds = get_bert_sentiment_dataset(df_bert)
    bert_ds.drop("movement", axis=1).to_pickle(CLOSE_BERT_PROCESSED_DATA)
    bert_ds.drop("close", axis=1).to_pickle(MVMNT_BERT_PROCESSED_DATA)

# run()
