import stockRnn


def main():
    # If datasets and trained BERT model present preprocesses
    # the data and redo the training with results saved to the output_data folder.
    stockRnn.price_predictors_trainers.run()

if __name__ == '__main__':
    main()